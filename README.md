Cette API, permet de récupérer les livres et leurs auteurs

Application 1 :
gestion des livres / auteurs (vous devrez ajouter le prix, éventuellement un stock)

Application 2,
C'est la gestion du panier, qui doit pouvoir contenir plusieurs livre et avoir un cout total. Pour se faire, l'application 2 devra s'intégrer à l'application 1 pour avoir les infos sur les livres (dont le prix et disponibilité en stock par exemple)

## Intialisation

1. Initialisation des applications (regroupe les commandes 1, 2, 3, 4, 5)

```bash
make init
```

## Commandes

1. Démarrage du serveur

```bash
make up
```

2. Installation des composants

```bash
make installs
```

3. Lancement des migrations

```bash
make migrations
```

4. Lancement des fixtures

```bash
make fixtures
```

5. Lancement de messenger

```bash
make messenger
```

6. Arret du serveur

```bash
make down
```

## URLs

\*. Créer un auteur - POST
http://127.0.0.1:8080/auteur/new
Les informations doivent-être au format JSON

\*. Créer un livre - POST
http://127.0.0.1:8080/livre/new
Les informations doivent-être au format JSON

\*. Consulter un livre - GET
http://127.0.0.1:8080/livre/{livre}

\*. Consulter les livres - GET
http://127.0.0.1:8080/livre/

\*. Créer un panier - GET
http://127.0.0.1:8081/panier/new

\*. Ajouter un livre dans un panier - GET
http://127.0.0.1:8081/livre/{panier}/add/{livreId}?qte={qte}

\*. Consulter un panier - GET
http://127.0.0.1:8081/panier/{panier}

\*. Retirer un livre du panier - DELETE
http://127.0.0.1:8081/livre/{panier}/{livreId}/remove

\*. Supprimer un panier - DELETE
http://127.0.0.1:8081/panier/{panier}
