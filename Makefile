user := $(shell id -u)
group := $(shell id -g)

.PHONY: up 

init: 
	make up
	make installs
	make migrations
	make fixtures
	make messenger

up:
	USER_ID=$(user) GROUP_ID=$(group) docker-compose up -d

down:
	docker-compose down

installs:
	docker-compose exec appli1 composer install
	docker-compose exec appli2 composer install

migrations:
	docker-compose exec appli1 bin/console doctrine:migrations:migrate
	docker-compose exec appli2 bin/console doctrine:migrations:migrate

fixtures:
	docker-compose exec appli1 bin/console doctrine:fixtures:load
	docker-compose exec appli2 bin/console doctrine:fixtures:load

messenger:
	docker-compose exec messenger bin/console messenger:consume async
