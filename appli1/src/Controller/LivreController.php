<?php

namespace App\Controller;

use App\Entity\Livre;
use App\Form\LivreType;
use App\Repository\LivreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/livre")
 */
class LivreController extends AbstractController
{
    /**
     * @Route("/", name="livre_index", methods={"GET"})
     */
    public function index(LivreRepository $livreRepository, SerializerInterface $serializer): Response
    {
        $livres = $livreRepository->findAll();
        // src : https://stackoverflow.com/questions/44286530/symfony-3-2-a-circular-reference-has-been-detected-configured-limit-1
        // Serialize your object in Json
        $jsonObject = $serializer->serialize($livres, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        // For instance, return a Response with encoded Json
        return new Response($jsonObject, Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/new", name="livre_new", methods={"POST"})
     * JSON   
     * {
     *  "titre": "test",
     *  "auteur": "test"
     *  "date_publication": "2020-01-01"
     * }
     */
    public function new(Request $request): Response
    {
        $livre = new Livre();
        $form = $this->createForm(LivreType::class,$livre);
        $data = json_decode($request->getContent(),true);
        $form->submit($data);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($livre);
            $entityManager->flush();

            return new Response('OK', Response::HTTP_CREATED);
        }
        return new Response('Page not found.', Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/{id}", name="livre_show", methods={"GET"})
     */
    public function show(Livre $livre, SerializerInterface $serializer): Response
    {
        // src : https://stackoverflow.com/questions/44286530/symfony-3-2-a-circular-reference-has-been-detected-configured-limit-1
        // Serialize your object in Json
        $jsonObject = $serializer->serialize($livre, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        // For instance, return a Response with encoded Json
        return new Response($jsonObject, Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/{id}/edit", name="livre_edit", methods={"POST"})
     * JSON
     * {
     *  "titre": "Livre 2",
     *  "auteur": "Auteur 2"
     * }
     */
    public function edit(Request $request, Livre $livre): Response
    {
        $form = $this->createForm(LivreType::class,$livre);
        $data = json_decode($request->getContent(),true);
        $form->submit($data);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($livre);
            $entityManager->flush();

            return new Response('OK', Response::HTTP_CREATED);
        }
        return new Response('Page not found.', Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/{id}", name="livre_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Livre $livre): Response
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($livre);
            $entityManager->flush();

            return new Response('OK', Response::HTTP_CREATED);
        } catch (\Throwable $th) {
            return new Response($th, Response::HTTP_NOT_FOUND);
        }
    }
}
