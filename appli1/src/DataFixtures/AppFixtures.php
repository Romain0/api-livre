<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Livre;
use App\Entity\LivreInfo;
use App\Entity\Auteur;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $auteur_1 = new Auteur();
        $auteur_1->setNom("Jules")
            ->setPrenom("Verne")
            ->setDateNaissance(new \DateTime('1828-02-08'));
        
        $auteur_2 = new Auteur();
        $auteur_2->setNom("Victor")
            ->setPrenom("Hugo")
            ->setDateNaissance(new \DateTime('1802-02-26'));

        $auteur_3 = new Auteur();
        $auteur_3->setNom("Guy")
            ->setPrenom("de Maupassant")
            ->setDateNaissance(new \DateTime('1850-08-05'));

        $livre_1 = new Livre();
        $livre_1->setTitre("Les Miserables")
            ->setDatePublication(new \DateTime("1862-01-01"))
            ->setAuteurId($auteur_2)
            ->setQte(10)
            ->setPrix(10);

        $livre_2 = new Livre();
        $livre_2->setTitre("Bel Ami")
            ->setDatePublication(new \DateTime("1885-01-01"))
            ->setAuteurId($auteur_3)
            ->setQte(10)
            ->setPrix(12);

        $livre_3 = new Livre();
        $livre_3->setTitre("Voyage au centre de la Terre")
            ->setDatePublication(new \DateTime("1872-01-01"))
            ->setAuteurId($auteur_1)
            ->setQte(10)
            ->setPrix(11);

        $livre_4 = new Livre();
        $livre_4->setTitre("Le Horla")
            ->setDatePublication(new \DateTime("1887-01-01"))
            ->setAuteurId($auteur_3)
            ->setQte(10)
            ->setPrix(9);

        $livre_5 = new Livre();
        $livre_5->setTitre("Boule de Suif")
            ->setDatePublication(new \DateTime("1880-04-15"))
            ->setAuteurId($auteur_3)
            ->setQte(10)
            ->setPrix(8);

        $livre_6 = new Livre();
        $livre_6->setTitre("La Maison Tellier")
            ->setDatePublication(new \DateTime("1881-01-01"))
            ->setAuteurId($auteur_3)
            ->setQte(10)
            ->setPrix(9.50);

        $livre_7 = new Livre();
        $livre_7->setTitre("Vingt Mille Lieues Sous Les Mers")
            ->setDatePublication(new \DateTime("1870-01-01"))
            ->setAuteurId($auteur_1)
            ->setQte(10)
            ->setPrix(11.50);

        $manager->persist($auteur_1);
        $manager->persist($auteur_2);
        $manager->persist($auteur_3);
        $manager->persist($livre_1);
        $manager->persist($livre_2);
        $manager->persist($livre_3);
        $manager->persist($livre_4);
        $manager->persist($livre_5);
        $manager->persist($livre_6);
        $manager->persist($livre_7);

        $manager->flush();
    }
}
