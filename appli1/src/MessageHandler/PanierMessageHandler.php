<?php
namespace App\MessageHandler;

use App\Entity\Livre;
use App\Repository\LivreRepository;
use App\Message\PanierMessage;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;

class PanierMessageHandler implements MessageHandlerInterface
{
    private $entityManager;
    private $livreRepository;

    public function __construct(EntityManagerInterface $entityManager, LivreRepository $livreRepository)
    {
        $this->entityManager = $entityManager;
        $this->livreRepository = $livreRepository;
    }

    public function __invoke(PanierMessage $message)
    {
        $panier = $this->livreRepository->find($message->getLivreId());

        $panier->setQte($panier->getQte() - $message->getQte());
        
        $this->entityManager->persist($panier);
        $this->entityManager->flush();
    }
}
