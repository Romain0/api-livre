<?php

namespace App\Entity;

use App\Repository\PanierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PanierRepository::class)
 * @ORM\Table(name="panier")
 */
class Panier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total;

    /**
     * @ORM\OneToMany(targetEntity=Livre::class, mappedBy="panier")
     */
    private $livreId;

    public function __construct()
    {
        $this->livreId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): ?self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return Collection|Livre[]
     */
    public function getLivreId(): Collection
    {
        return $this->livreId;
    }

    public function addLivreId(Livre $livreId): self
    {
        if (!$this->livreId->contains($livreId)) {
            $this->livreId[] = $livreId;
            $livreId->setPanier($this);
        }

        return $this;
    }

    public function removeLivreId(Livre $livreId): self
    {
        if ($this->livreId->removeElement($livreId)) {
            // set the owning side to null (unless already changed)
            if ($livreId->getPanier() === $this) {
                $livreId->setPanier(null);
            }
        }

        return $this;
    }
}
