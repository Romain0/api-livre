<?php
namespace App\Message;

class PanierMessage
{
    private $livreId;
    private $qte;

    public function __construct(int $livreId, int $qte)
    {
        $this->livreId = $livreId;
        $this->qte = $qte;
    }

    public function getLivreId(): int
    {
        return $this->livreId;
    }

    public function getQte(): int
    {
        return $this->qte;
    }
}
