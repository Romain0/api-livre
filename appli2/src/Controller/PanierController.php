<?php

namespace App\Controller;

use App\Entity\Panier;
use App\Entity\Livre;
use App\Form\PanierType;
use App\Form\LivreType;
use App\Repository\PanierRepository;
use App\Repository\LivreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/panier')]
class PanierController extends AbstractController
{
    #[Route('/', name: 'panier_index', methods: ['GET'])]
    public function index(PanierRepository $panierRepository, SerializerInterface $serializer): Response
    {
        //dump(file_get_contents("http://appli1:8080/livre-1.json"));
        $panier = $panierRepository->findAll();

        // Serialize your object in Json
        $jsonObject = $serializer->serialize($panier, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        // For instance, return a Response with encoded Json
        return new Response($jsonObject, Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    #[Route('/new', name: 'panier_new', methods: ['POST'])]
    public function new(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        try {
            $panier = new Panier();
            $entityManager->persist($panier);
            $entityManager->flush();

            return new Response(json_encode($panier->getId()), Response::HTTP_CREATED);
        } catch (\Throwable $th) {
            return new Response($th, Response::HTTP_NOT_FOUND);
        }
        
    }

    #[Route('/{id}', name: 'panier_show', methods: ['GET'])]
    public function show(Panier $panier, SerializerInterface $serializer): Response
    {
        // Serialize your object in Json
        $jsonObject = $serializer->serialize($panier, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        // For instance, return a Response with encoded Json
        return new Response($jsonObject, Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    #[Route('/{id}/edit', name: 'panier_edit', methods: ['POST'])]
    public function edit(Request $request, Panier $panier): Response
    {
        $form = $this->createForm(PanierType::class, $panier);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($panier);
            $entityManager->flush();

            return new Response('OK', Response::HTTP_CREATED);
        }
        return new Response('Page not found.', Response::HTTP_NOT_FOUND);
    }

    #[Route('/{id}', name: 'panier_delete', methods: ['DELETE'])]
    public function delete(Request $request, Panier $panier): Response
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($panier);
            $entityManager->flush();

            return new Response('OK', Response::HTTP_OK);
        } catch (\Throwable $th) {
            return new Response($th, Response::HTTP_NOT_FOUND);
        }
    }
}
