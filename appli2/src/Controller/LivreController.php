<?php

namespace App\Controller;

use App\Entity\Livre;
use App\Entity\Panier;
use App\Form\LivreType;
use App\Repository\LivreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use App\Message\PanierMessage;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

#[Route('/livre')]
class LivreController extends AbstractController
{
    #[Route('/{panier}/add/{livreId}', name: 'livre_new', methods: ['GET'])]
    public function add(Panier $panier, int $livreId, Request $request, LivreRepository $livreRepository, HttpClientInterface $client, MessageBusInterface $bus): Response
    {
        $qteDemandee = $request->query->get('qte');
        if($qteDemandee != null)
        {
            // call API 1
            $response = $client->request(
                'GET',
                $_ENV['URL_API_1'] . "livre/" . $livreId,
            );

            if($response->getStatusCode() == 200)
            {
                $contentType = $response->getHeaders()['content-type'][0];
                // $contentType = 'application/json'
                $content = $response->getContent();
                // $content = '{"id":521583, "name":"symfony-docs", ...}'
                $content = $response->toArray();
                // $content = ['id' => 521583, 'name' => 'symfony-docs', ...]

                $prixU = $content["prix"];
                $qteActuelle = $content["qte"];

                if($qteActuelle - $qteDemandee >= 0)
                {
                    $entityManager = $this->getDoctrine()->getManager();

                    $livre = $livreRepository->getLivre($panier->getId(), $livreId);
                    if($livre == null)
                    {
                        $livre = new Livre();
                        $livre->setLivreId($livreId);
                        $livre->setPanier($panier);
                    }
                    $livre->setQte($qteDemandee);
                    $livre->setPrixU($prixU);
                    $entityManager->persist($livre);

                    $panier->setTotal($panier->getTotal() + $prixU * $qteDemandee);
                    $entityManager->persist($panier);

                    try {
                        $entityManager->flush();
                    } catch (\Throwable $th) {
                        return new Response($th, Response::HTTP_NOT_FOUND);
                    }

                    $bus->dispatch(new PanierMessage($livreId, $qteDemandee));

                    return new Response('OK', Response::HTTP_CREATED);
                }
                else
                {
                    return new Response('The quantity cannot be greater than stock.', Response::HTTP_NOT_FOUND);
                }
            }
            else 
            {
                return new Response('Article ' . $livreId . ' doesnot exist.', Response::HTTP_NOT_FOUND);
            }
        }
        return new Response('Page not found.', Response::HTTP_NOT_FOUND);
    }

    #[Route('/{id}', name: 'livre_show', methods: ['GET'])]
    public function show(Livre $livre, SerializerInterface $serializer): Response
    {
        // Serialize your object in Json
        $jsonObject = $serializer->serialize($livre, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        // For instance, return a Response with encoded Json
        return new Response($jsonObject, Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    #[Route('/{panier}/{livreId}/remove', name: 'livre_delete', methods: ['DELETE'])]
    public function delete(Request $request, Panier $panier, int $livreId, LivreRepository $livreRepository): Response
    {
        $livre = $livreRepository->getLivre($panier->getId(), $livreId);
        if($livre != null) {
            try {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($livre);
                $entityManager->flush();

                return new Response('OK', Response::HTTP_OK);
            } catch (\Throwable $th) {
                return new Response($th, Response::HTTP_NOT_FOUND);
            }
        } else {
            return new Response('No value.', Response::HTTP_NOT_FOUND);
        }
    }
}
