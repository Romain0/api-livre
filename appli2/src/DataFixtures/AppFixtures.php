<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Livre;
use App\Entity\Panier;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($cpt=0; $cpt < 2; $cpt++) { 
            $panier[$cpt] = new Panier();
            $manager->persist($panier[$cpt]);
        }

        $manager->flush();
    }
}
