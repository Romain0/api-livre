<?php
namespace tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class API_1Test extends WebTestCase
{
    public function testShowLivresAPI_1()
    {
        $client = static::createClient();
        $client->request('GET', $_ENV['URL_API_1'] . "livre/");

        $code = intval($client->getResponse()->getStatusCode());

        $this->assertEquals(200, $code);
    }
}